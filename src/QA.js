import React, { Component } from "react";
import { connect } from "react-redux";

const QA = ({ failed }) => (
  <div>
    <h2>QA</h2>
    <h4>{failed} widgets failed QA</h4>
  </div>
);

const mapStateToProps = ({ warehouse: { failed } }) => ({
  failed
});
export default connect(mapStateToProps)(QA);
