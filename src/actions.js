import {
  ADD_WIDGET,
  GENERATE_ORDER,
  ORDER_MATERIALS,
  PACKAGE_ORDER,
  PRESENT_ERROR,
  SHIP_ORDER,
  DOWELS_NEEDED,
  SCREWS_NEEDED,
  WHEELS_NEEDED
} from "./constants";

import { push } from "react-router-redux";

export function attemptWidgetCreation() {
  return (dispatch, getState) => {
    const {
      warehouse: {
        materials: { dowel, screw, wheel }
      }
    } = getState();

    if (
      dowel.count >= DOWELS_NEEDED &&
      screw.count >= SCREWS_NEEDED &&
      wheel.count >= WHEELS_NEEDED
    ) {
      dispatch(addWidget());
    } else {
      dispatch(presentError("Not enough materials to create a widget"));
    }
  };
}

export const addWidget = () => ({
  date: Date.now(),
  type: ADD_WIDGET
});

export const generateOrder = () => ({
  date: Date.now(),
  type: GENERATE_ORDER
});

export const orderMaterials = () => ({
  type: ORDER_MATERIALS
});

export function checkOrderForPackaging() {
  return (dispatch, getState) => {
    const {
      warehouse: { orders, widgets }
    } = getState();
    const order = orders[0];
    if (order.widgets <= widgets.length) {
      dispatch(packageOrder(order));
      dispatch(push("/"));
    } else {
      dispatch(presentError("Not enough widgets to fill order!"));
    }
  };
}

export const packageOrder = order => ({
  type: PACKAGE_ORDER,
  order
});

export const shipOrder = () => ({
  type: SHIP_ORDER
});

export function presentError(message) {
  return {
    type: PRESENT_ERROR,
    message
  };
}
