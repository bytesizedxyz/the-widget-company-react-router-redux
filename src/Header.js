import React from "react";
import { connect } from "react-redux";

const Header = ({ error }) => (
  <div>
    <img className="logo" src="/logo.png" />
    <div style={{ margin: "2em" }}>
      {error && (
        <span style={{ background: "red", color: "white", padding: "1em" }}>
          {error}
        </span>
      )}
    </div>
  </div>
);

const mapStateToProps = ({ warehouse: { error } }) => ({
  error
});

export default connect(mapStateToProps)(Header);
