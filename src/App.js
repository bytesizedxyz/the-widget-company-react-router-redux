import React, { Component } from "react";
import "./index.css";
import "./normalize.css";

import Header from "./Header";

const App = ({ children }) => (
  <div>
    <Header />
    {children}
  </div>
);

export default App;
