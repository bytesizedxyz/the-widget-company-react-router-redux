import React, { Component } from "react";

import { checkOrderForPackaging } from "./actions";
import { connect } from "react-redux";

class Packaging extends Component {
  render() {
    const {
      warehouse: { orders, packaged },
      checkOrderForPackaging
    } = this.props;
    return (
      <div>
        <h2>Packaging</h2>
        <h4>{packaged} orders packaged</h4>
        {orders.length ? (
          <button onClick={checkOrderForPackaging}>Package order</button>
        ) : (
          <h4>No orders to package</h4>
        )}
      </div>
    );
  }
}

export default connect(
  state => state,
  { checkOrderForPackaging }
)(Packaging);
