import React, { Component } from "react";

import { attemptWidgetCreation, orderMaterials } from "./actions";
import { connect } from "react-redux";

class Inventory extends Component {
  render() {
    const {
      attemptWidgetCreation,
      orderMaterials,
      warehouse: {
        materials: { dowel, screw, wheel },
        widgets
      }
    } = this.props;

    return (
      <div>
        <h2>Inventory</h2>
        <h4>{widgets.length} widgets in inventory</h4>
        <button onClick={attemptWidgetCreation}>Manufacture widget</button>

        <h2>Materials</h2>
        <ul>
          <li>Dowel: {dowel.count}</li>
          <li>Screw: {screw.count}</li>
          <li>Wheel: {wheel.count}</li>
        </ul>
        <button onClick={orderMaterials}>Order raw materials</button>
      </div>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = {
  attemptWidgetCreation,
  orderMaterials
};

const component = connect(
  mapStateToProps,
  mapDispatchToProps
)(Inventory);

export default component;
