import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";

import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware } from "redux";
import {
  ConnectedRouter,
  routerReducer,
  routerMiddleware
} from "react-router-redux";
import { Link } from "react-router-dom";
import { Route } from "react-router";
import reduxLogger from "redux-logger";
import thunk from "redux-thunk";
import createHistory from "history/createBrowserHistory";

import reducer from "./reducer";
import routes from "./routes";

const history = createHistory();

const linkList = (
  <ul>
    {routes.map(route => (
      <li key={route.path}>
        <Link to={route.path}>{route.title}</Link>
      </li>
    ))}
  </ul>
);

const routeList = routes.map(({ component, exact, path }) => (
  <Route exact={exact || false} path={path} component={component} />
));

const store = createStore(
  combineReducers({
    router: routerReducer,
    warehouse: reducer
  }),
  applyMiddleware(routerMiddleware(history), reduxLogger, thunk)
);

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App>
        {linkList}
        {routeList}
      </App>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById("root"));
